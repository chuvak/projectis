package com.gmail.mihailmatiychin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogisticIsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogisticIsApplication.class, args);
    }
}

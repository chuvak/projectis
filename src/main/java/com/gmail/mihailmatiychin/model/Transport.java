package com.gmail.mihailmatiychin.model;

import javax.persistence.*;

@Entity
@Table(name = "transport")
public class Transport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String number;

    @Column(name = "carrying_сapacity", nullable = false)
    private int carryingCapacity;

    @Column(name = "fuel_consumption", nullable = false)
    private int fuelConsumption;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "depot")
    private Depot depot;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "transport")
    private LogisticOrder logisticOrderTo;


    public Transport(String number, int carryingCapacity, int fuelConsumption, Depot depot) {
        this.number = number;
        this.carryingCapacity = carryingCapacity;
        this.fuelConsumption = fuelConsumption;
        this.depot = depot;
    }

    public Transport(long id, String number, int carryingCapacity, int fuelConsumption, Depot depot) {
        this.id = id;
        this.number = number;
        this.carryingCapacity = carryingCapacity;
        this.fuelConsumption = fuelConsumption;
        this.depot = depot;
    }

    public Transport() {
    }

    public long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

}

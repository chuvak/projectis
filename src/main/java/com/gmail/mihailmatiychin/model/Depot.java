package com.gmail.mihailmatiychin.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "depot")
public class Depot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;
    @Column(nullable = false)
    private String city;
    @Column(nullable = false)
    private String address;
    @Column(nullable = false)
    private String phone;

    @OneToMany(mappedBy = "depot", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<User> users;

    @OneToMany(mappedBy = "depot", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transport> transport;

    @OneToMany(mappedBy = "from", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Route> from;

    @OneToMany(mappedBy = "to", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Route> to;

    @OneToMany(mappedBy = "from", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<LogisticOrder> logisticOrderFrom;

    @OneToMany(mappedBy = "to", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<LogisticOrder> logisticOrderTo;

    public Depot(String city, String address, String phone) {
        this.city = city;
        this.address = address;
        this.phone = phone;
        users = new ArrayList<>();
        transport = new ArrayList<>();
        from = new ArrayList<>();
        to = new ArrayList<>();
        logisticOrderFrom = new ArrayList<>();
        logisticOrderTo = new ArrayList<>();
    }

    public Depot(long id, String city, String address, String phone) {
        this.id = id;
        this.city = city;
        this.address = address;
        this.phone = phone;
        users = new ArrayList<>();
        transport = new ArrayList<>();
        from = new ArrayList<>();
        to = new ArrayList<>();
        logisticOrderFrom = new ArrayList<>();
        logisticOrderTo = new ArrayList<>();
    }

    public Depot() {
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Transport> getTransport() {
        return transport;
    }

    public void setTransport(List<Transport> transport) {
        this.transport = transport;
    }

    public List<Route> getFrom() {
        return from;
    }

    public void setFrom(List<Route> from) {
        this.from = from;
    }

    public List<Route> getTo() {
        return to;
    }

    public void setTo(List<Route> to) {
        this.to = to;
    }

    public List<LogisticOrder> getLogisticOrderFrom() {
        return logisticOrderFrom;
    }

    public void setLogisticOrderFrom(List<LogisticOrder> logisticOrderFrom) {
        this.logisticOrderFrom = logisticOrderFrom;
    }

    public List<LogisticOrder> getLogisticOrderTo() {
        return logisticOrderTo;
    }

    public void setLogisticOrderTo(List<LogisticOrder> logisticOrderTo) {
        this.logisticOrderTo = logisticOrderTo;
    }


}

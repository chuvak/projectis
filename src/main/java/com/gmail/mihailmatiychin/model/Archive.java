package com.gmail.mihailmatiychin.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "archive")
public class Archive {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "shiping_date", nullable = false)
    private Date shipingDate;

    @Column(name = "receiving_date", nullable = false)
    private Date receivingDate;

    @Column(name = "transport_number", nullable = false)
    private String transportNumber;

    @Column(name = "from_depot", nullable = false)
    private String from;

    @Column(name = "to_depot", nullable = false)
    private String to;

    @Column(nullable = false)
    private double cost;

    @Column(name = "declaration_number", nullable = false)
    private String declarationNumber;

    @Column(name = "order_cost", nullable = false)
    private double orderCost;

    @Column(name = "total_cost", nullable = false)
    private double totalCost;

    public Archive(Date shipingDate, Date receivingDate, String transportNumber, String from, String to, double cost,
                   String declarationNumber, double orderCost, double totalCost) {
        this.shipingDate = shipingDate;
        this.receivingDate = receivingDate;
        this.transportNumber = transportNumber;
        this.from = from;
        this.to = to;
        this.cost = cost;
        this.declarationNumber = declarationNumber;
        this.orderCost = orderCost;
        this.totalCost = totalCost;
    }

    public Archive() {
    }

    public long getId() {
        return id;
    }

    public Date getShipingDate() {
        return shipingDate;
    }

    public void setShipingDate(Date shipingDate) {
        this.shipingDate = shipingDate;
    }

    public Date getReceivingDate() {
        return receivingDate;
    }

    public void setReceivingDate(Date receivingDate) {
        this.receivingDate = receivingDate;
    }

    public String getTransportNumber() {
        return transportNumber;
    }

    public void setTransportNumber(String transportNumber) {
        this.transportNumber = transportNumber;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDeclarationNumber() {
        return declarationNumber;
    }

    public void setDeclarationNumber(String declarationNumber) {
        this.declarationNumber = declarationNumber;
    }

    public double getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(double orderCost) {
        this.orderCost = orderCost;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
}

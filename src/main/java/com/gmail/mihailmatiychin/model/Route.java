package com.gmail.mihailmatiychin.model;

import javax.persistence.*;

@Entity
@Table(name = "route")
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_depot")
    private Depot from;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_depot")
    private Depot to;

    private int distance;

    public Route(long id, Depot from, Depot to, int distance) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    public Route(Depot from, Depot to, int distance) {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    public Route() {
    }

    public long getId() {
        return id;
    }

    public Depot getFrom() {
        return from;
    }

    public void setFrom(Depot from) {
        this.from = from;
    }

    public Depot getTo() {
        return to;
    }

    public void setTo(Depot to) {
        this.to = to;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}

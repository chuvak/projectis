package com.gmail.mihailmatiychin.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "logistic_order")
public class LogisticOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "shipping_date", nullable = false)
    private Date shippingDate;

    @Column(name = "declaration_number", nullable = false)
    private String declarationNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transport")
    private Transport transport;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_depot")
    private Depot from;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_depot")
    private Depot to;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private double weight;

    @Column(nullable = false)
    private double cost;

    @Column(name = "order_cost", nullable = false)
    private double orderCost;

    @Column(name = "total_cost", nullable = false)
    private double totalCost;

    public LogisticOrder(Date shippingDate, String declarationNumber, Transport transport, Depot from, Depot to, String description, double weight,
                         double cost, double orderCost, double totalCost) {
        this.shippingDate = shippingDate;
        this.declarationNumber = declarationNumber;
        this.transport = transport;
        this.from = from;
        this.to = to;
        this.description = description;
        this.weight = weight;
        this.cost = cost;
        this.orderCost = orderCost;
        this.totalCost = totalCost;
    }

    public LogisticOrder() {
    }

    public long getId() {
        return id;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shipingDate) {
        this.shippingDate = shipingDate;
    }

    public String getDeclarationNumber() {
        return declarationNumber;
    }

    public void setDeclarationNumber(String declarationNumber) {
        this.declarationNumber = declarationNumber;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Depot getFrom() {
        return from;
    }

    public void setFrom(Depot from) {
        this.from = from;
    }

    public Depot getTo() {
        return to;
    }

    public void setTo(Depot to) {
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(double orderCost) {
        this.orderCost = orderCost;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
}

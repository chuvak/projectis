package com.gmail.mihailmatiychin.model;

import javax.persistence.*;

@Entity
@Table(name = "fuel_cost")
public class FuelCost {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "cost")
    private double fuelCost;

    public FuelCost(double fuelCost) {
        this.fuelCost = fuelCost;
    }

    public FuelCost() {
    }

    public long getId() {
        return id;
    }

    public double getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(double fuelCost) {
        this.fuelCost = fuelCost;
    }
}

package com.gmail.mihailmatiychin.model;

import javax.persistence.*;

@Entity
@Table(name = "cost_per_kilo")
public class CostPerKilo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "cost")
    private double costPerKilo;

    public CostPerKilo(double costPerKilo) {
        this.costPerKilo = costPerKilo;
    }

    public CostPerKilo() {
    }

    public Long getId() {
        return id;
    }

    public double getCostPerKilo() {
        return costPerKilo;
    }

    public void setCostPerKilo(double costPerKilo) {
        this.costPerKilo = costPerKilo;
    }
}

package com.gmail.mihailmatiychin.model;

import javax.persistence.*;

@Entity
@Table(name = "logistic_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "depot")
    private Depot depot;

    public User(String login, String password, String role, Depot depot) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.depot = depot;
    }

    public User(long id, String login, String password, String role, Depot depot) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
        this.depot = depot;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }
}

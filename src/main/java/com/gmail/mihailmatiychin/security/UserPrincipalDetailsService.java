package com.gmail.mihailmatiychin.security;


import com.gmail.mihailmatiychin.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = this.userRepository.findByLogin(login);
        if (user == null)
            throw new UsernameNotFoundException(login + " not found");

        UserPrincipal userPrincipal = new UserPrincipal(user);

        return userPrincipal;
    }
}

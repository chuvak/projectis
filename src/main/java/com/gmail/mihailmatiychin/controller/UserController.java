package com.gmail.mihailmatiychin.controller;

import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.LogisticOrder;
import com.gmail.mihailmatiychin.model.Transport;
import com.gmail.mihailmatiychin.model.User;
import com.gmail.mihailmatiychin.security.UserPrincipal;
import com.gmail.mihailmatiychin.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.ByteArrayInputStream;
import java.sql.Date;

@Controller
@RequestMapping("/user")
public class UserController {
    private static final int DEFAULT_ENTITY = -1;
    private static final int ITEMS_PER_PAGE = 10;
    @Autowired
    private LogisticOrderService logisticOrderService;
    @Autowired
    private UserService userService;
    @Autowired
    private DepotService depotService;
    @Autowired
    private RouteService routeService;
    @Autowired
    private TransportService transportService;
    @Autowired
    private FuelCostService fuelCostService;
    @Autowired
    private CostPerKiloService costPerKiloService;
    @Autowired
    private ArchiveService archiveService;
    @Autowired
    private PDFCreator pdfCreator;

    @RequestMapping("/orderManager")
    public String logisticOrder(Model model) {
        model.addAttribute("ordersListFrom", logisticOrderService.getListFrom(getDepotId()));

        return "user_order";
    }

    @RequestMapping(value = "/orderManager/print", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> logisticOrderGetDocument(@RequestParam(value = "id") long logisticOrderId) throws Exception {
        LogisticOrder logisticOrder = logisticOrderService.get(logisticOrderId);
        ByteArrayInputStream bis = pdfCreator.buildPdfDocument(logisticOrder);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=Declaration.pdf");
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @RequestMapping("/orderManager/new")
    public String logisticOrderAdd(Model model) {
        model.addAttribute("depots", routeService.getList(getDepotId()));
        model.addAttribute("transport", transportService.getByDepot(getDepotId()));

        return "user_order_add";
    }

    @RequestMapping(value = "/orderManager/add", method = RequestMethod.POST)
    public String logisticOrderAddNew(@RequestParam(value = "depot") long depotId,
                                      @RequestParam(value = "transport") long transportId, @RequestParam String description,
                                      @RequestParam String weight) {

        Transport transport = (transportId != DEFAULT_ENTITY) ? transportService.get(transportId): null;
        Depot depotTo = (depotId != DEFAULT_ENTITY) ? userService.findDepot(depotId) : null;
        try {
            logisticOrderService.save(transport, depotTo, description, Double.parseDouble(weight));
        } catch (Exception e) {
            return "redirect:/user/orderManager/new?error";
        }

        return "redirect:/user/orderManager";
    }

    @RequestMapping(value = "/orderManager/delete", method = RequestMethod.GET)
    public String logisticOrderDelete(@RequestParam(value = "id") long logisticOrder) {
        logisticOrderService.delete(logisticOrder);

        return "redirect:/user/orderManager";
    }

    @RequestMapping("/orderConfirmation")
    public String orderConfirmation(Model model) {
        model.addAttribute("ordersListTo", logisticOrderService.getListTo(getDepotId()));

        return "user_order_confirmation";
    }

    @RequestMapping("/orderConfirmation/search")
    public String orderConfirmationSearch(@RequestParam(value = "search") String search, Model model) {
        model.addAttribute("ordersListTo", logisticOrderService.get(search));

        return "user_order_confirmation";
    }

    @RequestMapping("/orderConfirmation/delete")
    public String orderConfirmationDeleter(@RequestParam(value = "id") long logisticOrder) {
        long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        logisticOrderService.deleteForArchive(logisticOrder, date);

        return "redirect:/user/orderConfirmation";
    }

    @RequestMapping("/logisticData")
    public String logisticData(Model model) {
        model.addAttribute("fuelCost", fuelCostService.get(1));
        model.addAttribute("costPerKilo", costPerKiloService.get(1));

        return "user_logistic_data";
    }

    @RequestMapping("/archive")
    public String archive(Model model, @RequestParam(required = false, defaultValue = "1") Integer page) {
        int pageCount;
        if (page < 1) {
            pageCount = page + 1;
        } else {
            pageCount = page;
        }
        long totalCount = archiveService.count();
        int start = (pageCount - 1) * ITEMS_PER_PAGE;
        if (start > totalCount) {
            pageCount = page - 1;
            start = (page - 2) * ITEMS_PER_PAGE;
        }
        model.addAttribute("archive", archiveService.getList(start, ITEMS_PER_PAGE));
        model.addAttribute("pages", pageCount);

        return "user_archive";
    }

    private long getDepotId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        String login = userPrincipal.getUsername();
        User user = userService.findByLogin(login);
        Depot depot = depotService.get(user.getDepot().getId());

        return depot.getId();
    }
}

package com.gmail.mihailmatiychin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class AppController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/admin")
    public String admin() {
        return "admin";
    }

    @RequestMapping("/user")
    public String user() {
        return "user";
    }

    @RequestMapping("/unauthorized")
    public String loginException() {
        return "unauthorized_exception";
    }

    @RequestMapping("/login")
    public String login() {
        return "index";
    }

}


package com.gmail.mihailmatiychin.controller;

import com.gmail.mihailmatiychin.model.*;
import com.gmail.mihailmatiychin.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private static final int DEFAULT_ID = -1;
    private static final int ITEMS_PER_PAGE = 10;
    @Autowired
    private UserService userService;
    @Autowired
    private TransportService transportService;
    @Autowired
    private RouteService routeService;
    @Autowired
    private DepotService depotService;
    @Autowired
    private FuelCostService fuelCostService;
    @Autowired
    private CostPerKiloService costPerKiloService;
    @Autowired
    private ArchiveService archiveService;

    @RequestMapping("/users")
    public String users(Model model, @RequestParam(required = false, defaultValue = "1") Integer page) {
        int pageCount;
        if (page < 1) {
            pageCount = page + 1;
        } else {
            pageCount = page;
        }
        long totalCount = userService.count();
        int start = (pageCount - 1) * ITEMS_PER_PAGE;
        if (start > totalCount) {
            pageCount = page - 1;
            start = (page - 2) * ITEMS_PER_PAGE;
        }
        model.addAttribute("userList", userService.getList(start, ITEMS_PER_PAGE));
        model.addAttribute("pages", pageCount);

        return "admin_users";
    }


    @RequestMapping("/users/new")
    public String usersAdd(Model model) {
        model.addAttribute("depots", depotService.getList());
        return "admin_users_add";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String usersAddNew(@RequestParam String login, @RequestParam String password,
                              @RequestParam(value = "role") String role, @RequestParam(value = "depot") long depotId) {

        Depot depot = (depotId != DEFAULT_ID) ? userService.findDepot(depotId) : null;
        try {
            userService.save(login, password, role, depot);
        } catch (Exception e) {
            return "redirect:/admin/users/new?error";
        }

        return "redirect:/admin/users";
    }

    @RequestMapping(value = "/users/delete", method = RequestMethod.GET)
    public String usersDelete(@RequestParam(value = "id") long userId) {
        try {
            userService.delete(userId);
        } catch (Exception e) {
            return "redirect:/admin/users?error";
        }

        return "redirect:/admin/users";
    }

    @RequestMapping(value = "/users/edit", method = RequestMethod.GET)
    public String usersEdit(@RequestParam(name = "id") long id, Model model) {
        User user = userService.get(id);
        List<Depot> depotList = depotService.getList();
        model.addAttribute("user", user);
        model.addAttribute("depot", depotList);

        return "admin_users_edit";
    }

    @RequestMapping(value = "/users/update", method = RequestMethod.POST)
    public String usersUpdate(@RequestParam(name = "id") long id, @RequestParam String login,
                              @RequestParam String password, @RequestParam(value = "role") String role,
                              @RequestParam(value = "depot") long depotId) {

        Depot depot = (depotId != DEFAULT_ID) ? userService.findDepot(depotId) : null;
        try {
            userService.update(id, login, password, role, depot);
        } catch (Exception e) {
            return "redirect:/admin/users/edit?error";
        }

        return "redirect:/admin/users";
    }

    @RequestMapping("/transport")
    public String transport(Model model, @RequestParam(required = false, defaultValue = "1") Integer page) {
        int pageCount;
        if (page < 1) {
            pageCount = page + 1;
        } else {
            pageCount = page;
        }
        long totalCount = transportService.count();
        int start = (pageCount - 1) * ITEMS_PER_PAGE;
        if (start > totalCount) {
            pageCount = page - 1;
            start = (page - 2) * ITEMS_PER_PAGE;
        }
        model.addAttribute("transportList", transportService.getList(start, ITEMS_PER_PAGE));
        model.addAttribute("pages", pageCount);

        return "admin_transport";
    }

    @RequestMapping("/transport/new")
    public String transportAdd(Model model) {
        model.addAttribute("depots", depotService.getList());

        return "admin_transport_add";
    }

    @RequestMapping(value = "/transport/add", method = RequestMethod.POST)
    public String transportAddNew(@RequestParam String transportNumber, @RequestParam String carryingCapacity,
                                  @RequestParam String fuelConsumption, @RequestParam(value = "depot") long depotId) {

        Depot depot = (depotId != DEFAULT_ID) ? userService.findDepot(depotId) : null;
        try {
            transportService.save(transportNumber, Integer.parseInt(carryingCapacity), Integer.parseInt(fuelConsumption), depot);
        } catch (Exception e) {
            return "redirect:/admin/transport/new?error";
        }

        return "redirect:/admin/transport";
    }

    @RequestMapping(value = "/transport/delete", method = RequestMethod.GET)
    public String transportDelete(@RequestParam(value = "id") long transportId) {
        transportService.delete(transportId);

        return "redirect:/admin/transport";
    }

    @RequestMapping(value = "/transport/edit", method = RequestMethod.GET)
    public String transportEdit(@RequestParam(name = "id") long id, Model model) {
        Transport transport = transportService.get(id);
        List<Depot> depotList = depotService.getList();
        model.addAttribute("transport", transport);
        model.addAttribute("depotList", depotList);

        return "admin_transport_edit";
    }

    @RequestMapping(value = "/transport/update", method = RequestMethod.POST)
    public String transportUpdate(@RequestParam(name = "id") long id, @RequestParam String number,
                                  @RequestParam int carryingCapacity, @RequestParam int fuelConsumption,
                                  @RequestParam(value = "depot") long depotId) {

        Depot depot = (depotId != DEFAULT_ID) ? transportService.findDepot(depotId) : null;
        try {
            transportService.update(id, number, carryingCapacity, fuelConsumption, depot);
        } catch (Exception e) {
            return "redirect:/admin/transport/edit?error";
        }

        return "redirect:/admin/transport";
    }

    @RequestMapping("/routes")
    public String routes(Model model, @RequestParam(required = false, defaultValue = "1") Integer page) {
        int pageCount;
        if (page < 1) {
            pageCount = page + 1;
        } else {
            pageCount = page;
        }
        long totalCount = routeService.count();
        int start = (pageCount - 1) * ITEMS_PER_PAGE;
        if (start > totalCount) {
            pageCount = page - 1;
            start = (page - 2) * ITEMS_PER_PAGE;
        }
        model.addAttribute("routesList", routeService.getList(start, ITEMS_PER_PAGE));
        model.addAttribute("pages", pageCount);

        return "admin_routes";
    }

    @RequestMapping("/routes/new")
    public String routesAdd(Model model) {
        model.addAttribute("depots", depotService.getList());

        return "admin_routes_add";
    }

    @RequestMapping(value = "/routes/add", method = RequestMethod.POST)
    public String routesAddNew(@RequestParam(value = "from") long depotIdFrom,
                               @RequestParam(value = "to") long depotIdTo,
                               @RequestParam String distance) {

        Depot depotFrom = (depotIdFrom != DEFAULT_ID) ? userService.findDepot(depotIdFrom) : null;
        Depot depotTo = (depotIdTo != DEFAULT_ID) ? userService.findDepot(depotIdTo) : null;
        try {
            routeService.save(depotFrom, depotTo, Integer.parseInt(distance));
        } catch (Exception e) {
            return "redirect:/admin/routes/new?error";
        }

        return "redirect:/admin/routes";
    }

    @RequestMapping(value = "/routes/delete", method = RequestMethod.GET)
    public String routesDelete(@RequestParam(value = "id") long routeId) {
        routeService.delete(routeId);

        return "redirect:/admin/routes";
    }

    @RequestMapping(value = "/routes/edit", method = RequestMethod.GET)
    public String routesEdit(@RequestParam(name = "id") long id, Model model) {
        Route route = routeService.get(id);
        List<Depot> depotList = depotService.getList();
        model.addAttribute("route", route);
        model.addAttribute("depotList", depotList);

        return "admin_routes_edit";
    }

    @RequestMapping(value = "/routes/update", method = RequestMethod.POST)
    public String routesUpdate(@RequestParam(name = "id") long id, @RequestParam(value = "depotFrom") long depotFromId,
                               @RequestParam(value = "depotFrom") long depotToId, @RequestParam String distance) {

        Depot depotFrom = (depotFromId != DEFAULT_ID) ? userService.findDepot(depotFromId) : null;
        Depot depotTo = (depotToId != DEFAULT_ID) ? userService.findDepot(depotToId) : null;
        try {
            routeService.update(id, depotFrom, depotTo, Integer.parseInt(distance));
        } catch (Exception e) {
            return "redirect:/admin/routes/edit?error";
        }

        return "redirect:/admin/routes";
    }

    @RequestMapping("/depots")
    public String depots(Model model, @RequestParam(required = false, defaultValue = "1") Integer page) {
        int pageCount;
        if (page < 1) {
            pageCount = page + 1;
        } else {
            pageCount = page;
        }
        long totalCount = depotService.count();
        int start = (pageCount - 1) * ITEMS_PER_PAGE;
        if (start > totalCount) {
            pageCount = page - 1;
            start = (page - 2) * ITEMS_PER_PAGE;
        }
        model.addAttribute("depotList", depotService.getList(start, ITEMS_PER_PAGE));
        model.addAttribute("pages", pageCount);

        return "admin_depots";
    }

    @RequestMapping("/depots/new")
    public String depotsAdd() {

        return "admin_depots_add";
    }

    @RequestMapping(value = "/depots/add", method = RequestMethod.POST)
    public String depotsAddNew(@RequestParam String city, @RequestParam String address, @RequestParam String phone) {
        try {
            depotService.save(city, address, phone);
        } catch (Exception e) {
            return "redirect:/admin/depots/new?error";
        }

        return "redirect:/admin/depots";
    }

    @RequestMapping(value = "/depots/delete", method = RequestMethod.GET)
    public String depotsDelete(@RequestParam(value = "id") long depotId) {
        depotService.delete(depotId);

        return "redirect:/admin/depots";
    }

    @RequestMapping(value = "/depots/edit", method = RequestMethod.GET)
    public String depotsEdit(@RequestParam(name = "id") long id, Model model) {
        Depot depot = depotService.get(id);
        model.addAttribute(depot);

        return "admin_depots_edit";
    }

    @RequestMapping(value = "/depots/update", method = RequestMethod.POST)
    public String depotsUpdate(@RequestParam(name = "id") long depotId, @RequestParam String city,
                               @RequestParam String address, @RequestParam String phone) {

        try {
            depotService.update(depotId, city, address, phone);
        } catch (Exception e) {
            return "redirect:/admin/depots/edit?error";
        }

        return "redirect:/admin/depots";
    }

    @RequestMapping("/logisticData")
    public String logisticData(Model model) {
        model.addAttribute("fuelCost", fuelCostService.get(1));
        model.addAttribute("costPerKilo", costPerKiloService.get(1));

        return "admin_logistic_data";
    }

    @RequestMapping("/logisticData/updateFuelCost")
    public String fuelCostUpdate(@RequestParam double fuelCost) {
        fuelCostService.update(fuelCost);

        return "redirect:/admin/logisticData";
    }

    @RequestMapping("/logisticData/updateCostPerKilo")
    public String costPerKiloUpdate(@RequestParam double costPerKilo) {
        costPerKiloService.update(costPerKilo);

        return "redirect:/admin/logisticData";
    }

    @RequestMapping("/archive")
    public String archive(Model model, @RequestParam(required = false, defaultValue = "1") Integer page) {
        int pageCount;
        if (page < 1) {
            pageCount = page + 1;
        } else {
            pageCount = page;
        }
        long totalCount = archiveService.count();
        int start = (pageCount - 1) * ITEMS_PER_PAGE;
        if (start > totalCount) {
            pageCount = page - 1;
            start = (page - 2) * ITEMS_PER_PAGE;
        }
        model.addAttribute("archive", archiveService.getList(start, ITEMS_PER_PAGE));
        model.addAttribute("pages", pageCount);

        return "admin_archive";
    }

}

package com.gmail.mihailmatiychin.service;

public interface CostCalculator {
    double calculateCost(int distance, int fuelConsumption);
    double calculateOrderCost(double weight);
}

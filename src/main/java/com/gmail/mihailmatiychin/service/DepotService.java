package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.Depot;

import java.util.List;

public interface DepotService {
    void save(String city, String address, String phone) throws Exception;
    void delete(long id);
    void update(long id, String city, String address, String phone) throws Exception;
    Depot get(long id);
    List<Depot> getAvailableDepots(long id);
    List<Depot> getList();
    List<Depot> getList(int start, int count);
    long count();
}

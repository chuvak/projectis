package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.CostPerKiloDAO;
import com.gmail.mihailmatiychin.model.CostPerKilo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CostPerKiloServiceImpl implements CostPerKiloService {
    @Autowired
    private CostPerKiloDAO costPerKiloDAO;

    @Override
    @Transactional
    public void save(CostPerKilo costPerKilo) {
        costPerKiloDAO.save(costPerKilo);
    }

    @Override
    @Transactional
    public void update(double costPerKilo) {
        costPerKiloDAO.update(costPerKilo);
    }

    @Override
    @Transactional
    public CostPerKilo get(long id) {
        return costPerKiloDAO.get(id);
    }


}

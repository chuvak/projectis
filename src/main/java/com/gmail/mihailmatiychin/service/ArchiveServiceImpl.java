package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.ArchiveDAO;
import com.gmail.mihailmatiychin.model.Archive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ArchiveServiceImpl implements ArchiveService {
    @Autowired
    private ArchiveDAO archiveDAO;

    @Override
    @Transactional
    public void save(Archive archive) {
        archiveDAO.save(archive);
    }

    @Override
    @Transactional
    public void delete(long id) {
        archiveDAO.delete(id);
    }

    @Override
    @Transactional
    public Archive get(long id) {
        return archiveDAO.get(id);
    }

    @Override
    @Transactional
    public List<Archive> getList(int start, int count) {
        return archiveDAO.getList(start, count);
    }

    @Override
    public long count() {
        return archiveDAO.count();
    }
}

package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.Transport;

import java.util.List;

public interface TransportService {
    void save(String transportNumber, int carryingCapacity, int fuelConsumption, Depot depot) throws Exception;
    void delete(long id);
    void update(long id, String transportNumber, int carryingCapacity, int fuelConsumption, Depot depot) throws Exception;
    Transport get(long id);
    Depot findDepot(long id);
    List<Transport>getByDepot(long id);
    List<Transport> getList(int start, int count);
    long count();
}

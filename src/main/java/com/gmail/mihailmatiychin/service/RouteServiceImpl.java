package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.RouteDAO;
import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RouteServiceImpl implements RouteService {
    @Autowired
    private RouteDAO routeDAO;

    @Override
    @Transactional
    public void save(Depot depotFrom, Depot depotTo, int distance) throws Exception{
        if (depotFrom != null && depotTo != null && depotFrom != depotTo && distance > 0) {

            Route route = new Route(depotFrom, depotTo, distance);
            routeDAO.save(route);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional
    public void delete(long id) {
        routeDAO.delete(id);
    }

    @Override
    @Transactional
    public void update(long id, Depot depotFrom, Depot depotTo, int distance) throws Exception{
        if (depotFrom != null && depotTo != null && depotFrom != depotTo && distance > 0) {

            Route route = new Route(id, depotFrom, depotTo, distance);
            routeDAO.update(route);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional
    public Route get(long id) {
        return routeDAO.get(id);
    }

    @Override
    public Route get(long idFrom, long idTo) {
        return routeDAO.get(idFrom, idTo);
    }

    @Override
    @Transactional
    public List<Route> getList(int start, int count) {
        return routeDAO.getList(start, count);
    }

    @Override
    @Transactional
    public List<Route> getList(long id) {
        return routeDAO.getList(id);
    }

    @Override
    public long count() {
        return routeDAO.count();
    }
}

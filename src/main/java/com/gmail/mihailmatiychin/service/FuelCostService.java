package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.FuelCost;

public interface FuelCostService {
    void save(FuelCost fuelCost);
    void update(double fuelCost);
    FuelCost get(long id);
}

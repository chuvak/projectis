package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.DepotDAO;
import com.gmail.mihailmatiychin.model.Depot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DepotServiceImpl implements DepotService {
    @Autowired
    private DepotDAO depotDAO;

    @Override
    @Transactional
    public void save(String city, String address, String phone) throws Exception{
        if (!city.equals("") && city.matches("[a-zA-Z]*") && !address.equals("") && !phone.equals("") && phone.matches("[0-9]*")) {

            Depot depot = new Depot(city, address, phone);
            depotDAO.save(depot);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional
    public void delete(long id) {
        depotDAO.delete(id);
    }

    @Override
    @Transactional
    public void update(long id, String city, String address, String phone) throws Exception{
        if (!city.equals("") && city.matches("[a-zA-Z]*") && !address.equals("") && !phone.equals("")
                && phone.matches("[0-9]*") && phone.length() == 10) {

            Depot depot = new Depot(id, city, address, phone);
            depotDAO.update(depot);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Depot get(long id) {
        return depotDAO.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Depot> getAvailableDepots(long id) {
        return depotDAO.getAvailableDepots(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Depot> getList() {
        return depotDAO.getList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Depot> getList(int start, int count) {
        return depotDAO.getList(start, count);
    }

    @Override
    public long count() {
        return depotDAO.count();
    }
}

package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.CostPerKilo;

public interface CostPerKiloService {
    void save(CostPerKilo costPerKilo);
    void update(double costPerKilo);
    CostPerKilo get(long id);
}

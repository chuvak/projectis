package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.User;

import java.util.List;

public interface UserService {
    void save(String login, String password,  String role, Depot depot) throws Exception;
    void delete(long id) throws Exception;
    void update(long id, String login, String password,  String role, Depot depot) throws Exception;
    User get(long id);
    Depot findDepot(long id);
    User findByLogin(String login);
    List<User> getList(int start, int count);
    long count();
}

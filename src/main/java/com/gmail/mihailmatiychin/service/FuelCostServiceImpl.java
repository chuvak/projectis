package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.FuelCostDAO;
import com.gmail.mihailmatiychin.model.FuelCost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FuelCostServiceImpl implements FuelCostService {
    @Autowired
    private FuelCostDAO fuelCostDAO;

    @Override
    @Transactional
    public void save(FuelCost fuelCost) {
        fuelCostDAO.save(fuelCost);
    }

    @Override
    @Transactional
    public void update(double fuelCost) {
        fuelCostDAO.update(fuelCost);
    }

    @Override
    @Transactional
    public FuelCost get(long id) {
        return fuelCostDAO.get(id);
    }

}

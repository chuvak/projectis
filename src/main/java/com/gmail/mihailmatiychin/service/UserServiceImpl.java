package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.DepotDAO;
import com.gmail.mihailmatiychin.dao.UserDAO;
import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private DepotDAO depotDAO;

    @Override
    @Transactional
    public void save(String login, String password, String role, Depot depot) throws Exception {
        if (!login.equals("") && !password.equals("") && login.matches("^[a-zA-Z0-9]*$")
                && password.matches("^[a-zA-Z0-9]*$") && depot != null) {
            if (userDAO.checkUser(login) != 0) {
                throw new Exception();
            }
            User user = new User(login, password, role, depot);
            userDAO.save(user);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional
    public void delete(long id) throws Exception {
        if (get(id).getRole().equals("ADMIN")) {
            if (userDAO.countRole() == 1) {
                throw new Exception();
            } else {
                userDAO.delete(id);
            }
        } else {
            userDAO.delete(id);
        }
    }

    @Override
    @Transactional
    public void update(long id, String login, String password, String role, Depot depot) throws Exception {
        if (!login.equals("") && !password.equals("") && login.matches("^[a-zA-Z0-9]*$")
                && password.matches("^[a-zA-Z0-9]*$") && depot != null) {
            User user = new User(id, login, password, role, depot);
            userDAO.update(user);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public User get(long id) {
        return userDAO.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Depot findDepot(long id) {
        return depotDAO.get(id);
    }

    @Override
    @Transactional
    public User findByLogin(String login) {
        return userDAO.findByLogin(login);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getList(int start, int count) {
        return userDAO.getList(start, count);
    }

    @Override
    public long count() {
        return userDAO.count();
    }
}

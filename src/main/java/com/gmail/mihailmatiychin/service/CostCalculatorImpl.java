package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.CostPerKiloDAO;
import com.gmail.mihailmatiychin.dao.FuelCostDAO;
import com.gmail.mihailmatiychin.dao.RouteDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class CostCalculatorImpl implements CostCalculator {
    @Autowired
    private FuelCostDAO fuelCostDAO;
    @Autowired
    private CostPerKiloDAO costPerKiloDAO;

    @Override
    @Transactional
    public double calculateCost(int distance, int fuelConsumption) {
        int costA = distance / fuelConsumption;
        double costB = distance % fuelConsumption;
        double cost = costA + costB;
        return round(cost * fuelCostDAO.get(1).getFuelCost() );
    }

    @Override
    @Transactional
    public double calculateOrderCost(double weight) {
        return round(costPerKiloDAO.get(2).getCostPerKilo() * weight);
    }

    private double round(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}

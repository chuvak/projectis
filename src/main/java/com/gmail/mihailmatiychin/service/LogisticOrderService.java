package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.LogisticOrder;
import com.gmail.mihailmatiychin.model.Transport;

import java.sql.Date;
import java.util.List;

public interface LogisticOrderService {
    void save(Transport transport, Depot depotTo, String description, double weight) throws Exception;
    void delete(long id);
    void deleteForArchive(long id, Date date);
    LogisticOrder get(long id);
    List<LogisticOrder> get(String search);
    List<LogisticOrder> getListFrom(long id);
    List<LogisticOrder> getListTo(long id);
    List<LogisticOrder> getList();
    long count();
    Depot getUserDepot();
}

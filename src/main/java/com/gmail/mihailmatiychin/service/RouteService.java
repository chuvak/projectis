package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.Route;

import java.util.List;

public interface RouteService {
    void save(Depot depotFrom, Depot depotTo, int distance) throws Exception;
    void delete(long id);
    void update(long id, Depot depotFrom, Depot depotTo, int distance) throws Exception;
    Route get(long id);
    Route get(long idFrom, long idTo);
    List<Route> getList(int start, int count);
    List<Route> getList(long id);
    long count();
}

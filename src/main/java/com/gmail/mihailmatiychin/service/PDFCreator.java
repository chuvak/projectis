package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.model.LogisticOrder;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PDFCreator {

    public ByteArrayInputStream buildPdfDocument(LogisticOrder logisticOrder) throws IOException {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(60);
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Declaration number"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(logisticOrder.getDeclarationNumber()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Shiping Date"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(String.valueOf(logisticOrder.getShippingDate())));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("From"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(logisticOrder.getFrom().getCity() + ", " + logisticOrder.getFrom().getAddress()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("To"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(logisticOrder.getTo().getCity() + ", " + logisticOrder.getTo().getAddress()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Description"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(logisticOrder.getDescription()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Weight"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(String.valueOf(logisticOrder.getWeight())));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Cost"));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(String.valueOf(logisticOrder.getTotalCost())));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPaddingLeft(5);
            table.addCell(cell);
            try {
                PdfWriter.getInstance(document, out);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            document.open();
            document.add(table);
            document.close();
        } catch (DocumentException ex) {
            Logger.getLogger(PDFCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }
}

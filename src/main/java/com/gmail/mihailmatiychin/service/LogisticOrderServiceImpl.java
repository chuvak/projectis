package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.ArchiveDAO;
import com.gmail.mihailmatiychin.dao.LogisticOrderDAO;
import com.gmail.mihailmatiychin.model.*;
import com.gmail.mihailmatiychin.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;
import java.util.Random;

@Service
public class LogisticOrderServiceImpl implements LogisticOrderService {
    @Autowired
    private LogisticOrderDAO logisticOrderDAO;
    @Autowired
    private ArchiveDAO archiveDAO;
    @Autowired
    private CostCalculator costCalculator;
    @Autowired
    private RouteService routeService;
    @Autowired
    private ArchiveService archiveService;
    @Autowired
    private UserService userService;
    @Autowired
    private DepotService depotService;

    @Override
    @Transactional
    public void save(Transport transport, Depot depotTo, String description, double weight) throws Exception {
        if (weight <= 0 || transport == null || depotTo == null || transport.getCarryingCapacity() < weight) {
            throw new Exception();
        }
        Random random = new Random();
        long declarationNumber = archiveService.count() + 1000 + random.nextInt(100);
        long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        double cost = costCalculator.calculateCost(routeService.get(getUserDepot().getId(), depotTo.getId()).getDistance(),
                transport.getFuelConsumption());

        double orderCost = costCalculator.calculateOrderCost(weight);

        LogisticOrder logisticOrder = new LogisticOrder(date, String.valueOf(declarationNumber), transport, getUserDepot(),
                depotTo, description, weight, cost, orderCost, cost + orderCost);
        logisticOrderDAO.save(logisticOrder);
    }

    @Override
    @Transactional
    public void delete(long id) {
        logisticOrderDAO.delete(id);
    }

    @Override
    @Transactional
    public void deleteForArchive(long id, Date date) {
        LogisticOrder logisticOrder = logisticOrderDAO.get(id);
        Archive archive = new Archive();
        archive.setShipingDate(logisticOrder.getShippingDate());
        archive.setReceivingDate(date);
        archive.setTransportNumber(logisticOrder.getTransport().getNumber());
        archive.setFrom(logisticOrder.getFrom().getCity() + ", " + logisticOrder.getFrom().getAddress());
        archive.setTo(logisticOrder.getTo().getCity() + ", " + logisticOrder.getTo().getAddress());
        archive.setCost(logisticOrder.getCost());
        archive.setDeclarationNumber(logisticOrder.getDeclarationNumber());
        archive.setOrderCost(logisticOrder.getOrderCost());
        archive.setTotalCost(logisticOrder.getTotalCost());
        archiveDAO.save(archive);
        logisticOrderDAO.delete(id);
    }

    @Override
    @Transactional
    public LogisticOrder get(long id) {
        return logisticOrderDAO.get(id);
    }

    @Override
    public List<LogisticOrder> get(String search) {
        return logisticOrderDAO.get(search);
    }

    @Override
    @Transactional
    public List<LogisticOrder> getListFrom(long id) {
        return logisticOrderDAO.getListFrom(id);
    }

    @Override
    @Transactional
    public List<LogisticOrder> getListTo(long id) {
        return logisticOrderDAO.getListTo(id);
    }

    @Override
    @Transactional
    public List<LogisticOrder> getList() {
        return logisticOrderDAO.getList();
    }

    @Override
    public long count() {
        return logisticOrderDAO.count();
    }

    @Override
    public Depot getUserDepot() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        String login = userPrincipal.getUsername();
        User user = userService.findByLogin(login);
        return depotService.get(userPrincipal.getId()/*.getDepot().getId()*/);
    }
}

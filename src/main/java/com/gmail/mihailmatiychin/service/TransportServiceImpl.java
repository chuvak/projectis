package com.gmail.mihailmatiychin.service;

import com.gmail.mihailmatiychin.dao.DepotDAO;
import com.gmail.mihailmatiychin.dao.TransportDAO;
import com.gmail.mihailmatiychin.model.Depot;
import com.gmail.mihailmatiychin.model.Transport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public class TransportServiceImpl implements TransportService {
    @Autowired
    private TransportDAO transportDAO;
    @Autowired
    private DepotDAO depotDAO;

    @Override
    @Transactional
    public void save(String transportNumber, int carryingCapacity, int fuelConsumption, Depot depot) throws Exception {
        if (!transportNumber.equals("") && transportNumber.matches("[a-zA-Z0-9]*") && carryingCapacity > 0 && fuelConsumption > 0 && depot != null) {

            Transport transport = new Transport(transportNumber, carryingCapacity, fuelConsumption, depot);
            transportDAO.save(transport);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional
    public void delete(long id) {
        transportDAO.delete(id);
    }

    @Override
    @Transactional
    public void update(long id, String transportNumber, int carryingCapacity, int fuelConsumption, Depot depot) throws Exception{
        if (!transportNumber.equals("") && transportNumber.matches("^[a-zA-Z0-9]*$") && carryingCapacity > 0
                && fuelConsumption > 0 && depot != null) {
            Transport transport = new Transport(id, transportNumber, carryingCapacity, fuelConsumption, depot);
            transportDAO.update(transport);
        } else {
            throw new Exception();
        }
    }

    @Override
    @Transactional
    public Transport get(long id) {
        return transportDAO.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Depot findDepot(long id) {
        return depotDAO.get(id);
    }

    @Override
    public List<Transport> getByDepot(long id) {
        return transportDAO.getByDepot(id);
    }

    @Override
    @Transactional
    public List<Transport> getList(int start, int count) {
        return transportDAO.getList(start, count);
    }

    @Override
    public long count() {
        return transportDAO.count();
    }
}

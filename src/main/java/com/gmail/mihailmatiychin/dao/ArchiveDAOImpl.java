package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Archive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class ArchiveDAOImpl implements ArchiveDAO{
    @Autowired
    private EntityManager entityManager;

    @Override
    public void save(Archive archive) {
        entityManager.persist(archive);
    }

    @Override
    public void delete(long id) {
        Archive archive = entityManager.getReference(Archive.class, id);
        entityManager.remove(archive);
    }

    @Override
    public Archive get(long id) {
        return entityManager.find(Archive.class, id);
    }

    @Override
    public List<Archive> getList(int start, int count) {
        TypedQuery<Archive> query = entityManager.createQuery("SELECT c FROM Archive c ORDER BY c.receivingDate DESC ", Archive.class);
        query.setFirstResult(start);
        query.setMaxResults(count);
        return query.getResultList();
    }

    @Override
    public long count() {
        TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(c) FROM Archive c", Long.class);
        return query.getSingleResult();
    }
}

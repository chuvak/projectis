package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.LogisticOrder;
import com.gmail.mihailmatiychin.model.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class LogisticOrderDAOImpl implements LogisticOrderDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(LogisticOrder logisticOrder) {
        entityManager.merge(logisticOrder);
    }

    @Override
    public void delete(long id) {
        LogisticOrder logisticOrder = entityManager.getReference(LogisticOrder.class, id);
        entityManager.remove(logisticOrder);
    }

    @Override
    public LogisticOrder get(long id) {
        return entityManager.find(LogisticOrder.class, id);
    }

    @Override
    public List<LogisticOrder> get(String search) {
        TypedQuery<LogisticOrder> query = entityManager.createQuery("Select c from LogisticOrder c where c.declarationNumber = :search", LogisticOrder.class);
        return query.setParameter("search", search).getResultList();
    }

    @Override
    public List<LogisticOrder> getListFrom(long id) {
//        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        CriteriaQuery<LogisticOrder> cq = cb.createQuery(LogisticOrder.class);
//        Root<LogisticOrder> order = cq.from(LogisticOrder.class);
//        Predicate depotId = cb.equal(order.get("from"), id);
//        cq.orderBy(cb.desc(order.get("shippingDate"))).where(depotId);
//        TypedQuery<LogisticOrder> query = entityManager.createQuery(cq);
        TypedQuery<LogisticOrder> query = entityManager.createQuery("Select c from LogisticOrder c where c.from.id = :id order by c.shippingDate desc ", LogisticOrder.class);
        return query.setParameter("id", id).getResultList();
    }

    @Override
    public List<LogisticOrder> getListTo(long id) {
//        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        CriteriaQuery<LogisticOrder> cq = cb.createQuery(LogisticOrder.class);
//        Root<LogisticOrder> order = cq.from(LogisticOrder.class);
//        Predicate depotId = cb.equal(order.get("to"), id);
//        cq.orderBy(cb.desc(order.get("shippingDate"))).where(depotId);
//        TypedQuery<LogisticOrder> query = entityManager.createQuery(cq);
        TypedQuery<LogisticOrder> query = entityManager.createQuery("Select c from LogisticOrder c where c.to.id = :id order by c.shippingDate desc ", LogisticOrder.class);
        return query.setParameter("id", id).getResultList();
    }

    @Override
    public List<LogisticOrder> getList() {
        TypedQuery<LogisticOrder> query = entityManager.createQuery("SELECT c FROM LogisticOrder c ORDER BY c.shippingDate DESC", LogisticOrder.class);
        return query.getResultList();
    }

    @Override
    public long count() {
        TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(c) FROM LogisticOrder c", Long.class);
        return query.getSingleResult();
    }
}

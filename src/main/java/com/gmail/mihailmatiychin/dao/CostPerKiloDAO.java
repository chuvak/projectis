package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.CostPerKilo;

public interface CostPerKiloDAO {
    void save(CostPerKilo costPerKilo);
    void update(double costPerKilo);
    CostPerKilo get(long id);
}

package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.LogisticOrder;

import java.util.List;

public interface LogisticOrderDAO {
    void save(LogisticOrder logisticOrder);
    void delete(long id);
    LogisticOrder get(long id);
    List<LogisticOrder> get(String search);
    List<LogisticOrder> getListFrom(long id);
    List<LogisticOrder> getListTo(long id);
    List<LogisticOrder> getList();
    long count();
}

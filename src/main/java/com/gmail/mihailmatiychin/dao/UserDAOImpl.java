package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        String password = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(password);
        entityManager.persist(user);
    }

    @Override
    public void delete(long id) {
        User user = entityManager.getReference(User.class, id);
        entityManager.remove(user);
    }

    @Override
    public void update(User user) {
        User updateUser = get(user.getId());
        updateUser.setLogin(user.getLogin());
        updateUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        updateUser.setRole(user.getRole());
        updateUser.setDepot(user.getDepot());
        entityManager.flush();
    }

    @Override
    public User get(long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User findByLogin(String login) {
        TypedQuery<User> query = entityManager.createQuery("Select c from User c where c.login = :login", User.class);
        return query.setParameter("login", login).getSingleResult();
    }

    @Override
    public long checkUser(String login) {
        TypedQuery<Long> query = entityManager.createQuery("Select COUNT(c) from User c where c.login = :login", Long.class);
        return query.setParameter("login", login).getSingleResult();
    }

    @Override
    public List<User> getList(int start, int count) {
        TypedQuery<User> query = entityManager.createQuery("Select c from User c order by c.role asc", User.class);
        query.setFirstResult(start);
        query.setMaxResults(count);
        return query.getResultList();
    }

    @Override
    public long count() {
        TypedQuery<Long> query = entityManager.createQuery("Select COUNT(c) from User c", Long.class);
        return query.getSingleResult();
    }

    @Override
    public long countRole() {
        TypedQuery<Long> query = entityManager.createQuery("Select COUNT(c) from User c where c.role = 'ADMIN'", Long.class);
        return query.getSingleResult();
    }
}

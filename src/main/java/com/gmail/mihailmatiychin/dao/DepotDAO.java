package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Depot;

import java.util.List;

public interface DepotDAO {
    void save(Depot depot);
    void delete(long id);
    void update(Depot depot);
    Depot get(long id);
    List<Depot> getAvailableDepots(long id);
    List<Depot> getList();
    List<Depot> getList(int start, int count);
    long count();

}

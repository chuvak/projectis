package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Archive;

import java.util.List;

public interface ArchiveDAO {
    void save(Archive archive);
    void delete(long id);
    Archive get(long id);
    List<Archive> getList(int start, int count);
    long count();
}

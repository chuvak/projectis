package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Route;
import java.util.List;

public interface RouteDAO {
    void save(Route route);
    void delete(long id);
    void update(Route route);
    Route get(long id);
    Route get(long idFrom, long idTo);
    List<Route> getList(int start, int count);
    List<Route> getList(long id);
    long count();
}

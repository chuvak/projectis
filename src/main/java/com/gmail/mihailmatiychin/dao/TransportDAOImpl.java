package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Transport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class TransportDAOImpl implements TransportDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Transport transport) {
        entityManager.persist(transport);
    }

    @Override
    public void delete(long id) {
        Transport transport = entityManager.getReference(Transport.class, id);
        entityManager.remove(transport);
    }

    @Override
    public void update(Transport transport) {
        Transport updateTransport = get(transport.getId());
        updateTransport.setNumber(transport.getNumber());
        updateTransport.setFuelConsumption(transport.getFuelConsumption());
        updateTransport.setCarryingCapacity(transport.getCarryingCapacity());
        updateTransport.setDepot(transport.getDepot());
        entityManager.flush();
    }

    @Override
    public Transport get(long id) {
        return entityManager.find(Transport.class, id);
    }

    @Override
    public List<Transport> getByDepot(long id) {
        TypedQuery<Transport> query = entityManager.createQuery("Select c from Transport c where c.depot.id = :id", Transport.class);
        return query.setParameter("id", id).getResultList();
    }

    @Override
    public List<Transport> getList(int start, int count) {
        TypedQuery<Transport> query = entityManager.createQuery("Select c from Transport c order by c.depot.city asc", Transport.class);
        query.setFirstResult(start);
        query.setMaxResults(count);
        return query.getResultList();
    }

    @Override
    public long count() {
        TypedQuery<Long> query = entityManager.createQuery("Select COUNT(c) from Transport c", Long.class);
        return query.getSingleResult();
    }
}

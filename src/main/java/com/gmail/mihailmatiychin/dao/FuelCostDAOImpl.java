package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.FuelCost;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class FuelCostDAOImpl implements FuelCostDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(FuelCost fuelCost) {
        entityManager.persist(fuelCost);
    }

    @Override
    public void update(double fuelCost) {
        FuelCost updateFuelCost = get(1);
        updateFuelCost.setFuelCost(fuelCost);
        entityManager.flush();
    }

    @Override
    public FuelCost get(long id) {
        return entityManager.find(FuelCost.class, id);
    }


}

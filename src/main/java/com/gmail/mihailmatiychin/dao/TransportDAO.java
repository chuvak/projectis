package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Transport;

import java.util.List;

public interface TransportDAO {
    void save(Transport transport);
    void delete(long id);
    void update(Transport transport);
    Transport get(long id);
    List<Transport>getByDepot(long id);
    List<Transport> getList(int start, int count);
    long count();
}

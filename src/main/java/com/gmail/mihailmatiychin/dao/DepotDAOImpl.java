package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Depot;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class DepotDAOImpl implements DepotDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Depot depot) {
        entityManager.persist(depot);
    }

    @Override
    public void delete(long id) {
        Depot c = entityManager.getReference(Depot.class, id);
        entityManager.remove(c);
    }

    @Override
    public void update(Depot depot) {
        Depot updateDepot = get(depot.getId());
        updateDepot.setCity(depot.getCity());
        updateDepot.setAddress(depot.getAddress());
        updateDepot.setPhone(depot.getPhone());
        entityManager.flush();
    }

    @Override
    public Depot get(long id) {
        return entityManager.find(Depot.class, id);
    }

    @Override
    public List<Depot> getAvailableDepots(long id) {
        TypedQuery<Depot> query = entityManager.createQuery("Select c from Depot c where c.id <> :id", Depot.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

    @Override
    public List<Depot> getList() {
        TypedQuery<Depot> query = entityManager.createQuery("Select c from Depot c order by c.city asc", Depot.class);
        return query.getResultList();
    }

    @Override
    public List<Depot> getList(int start, int count) {
        TypedQuery<Depot> query = entityManager.createQuery("Select c from Depot c order by c.city asc", Depot.class);
        query.setFirstResult(start);
        query.setMaxResults(count);
        return query.getResultList();
    }

    @Override
    public long count() {
        TypedQuery<Long> query = entityManager.createQuery("Select COUNT(c) from Depot c", Long.class);
        return query.getSingleResult();
    }
}

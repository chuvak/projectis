package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.FuelCost;

import java.util.List;

public interface FuelCostDAO {
    void save(FuelCost fuelCost);
    void update(double fuelCost);
    FuelCost get(long id);
}

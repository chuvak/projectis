package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.User;

import java.util.List;

public interface UserDAO {
    void save(User depot);
    void delete(long id);
    void update(User depot);
    User get(long id);
    User findByLogin(String login);
    long checkUser(String login);
    List<User> getList(int start, int count);
    long count();
    long countRole();
}

package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.CostPerKilo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CostPerKiloDAOImpl implements CostPerKiloDAO{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(CostPerKilo costPerKilo) {
        entityManager.persist(costPerKilo);
    }

    @Override
    public void update(double costPerKilo) {
        CostPerKilo updateCostPerKilo = get(2);
        updateCostPerKilo.setCostPerKilo(costPerKilo);
        entityManager.flush();
    }

    @Override
    public CostPerKilo get(long id) {
        return entityManager.find(CostPerKilo.class, id);
    }

}

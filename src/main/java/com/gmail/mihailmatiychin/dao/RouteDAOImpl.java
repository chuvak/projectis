package com.gmail.mihailmatiychin.dao;

import com.gmail.mihailmatiychin.model.Route;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class RouteDAOImpl implements RouteDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Route route) {
        entityManager.persist(route);
    }

    @Override
    public void delete(long id) {
        Route route = entityManager.getReference(Route.class, id);
        entityManager.remove(route);
    }

    @Override
    public void update(Route route) {
        Route updateRoute = get(route.getId());
        updateRoute.setFrom(route.getFrom());
        updateRoute.setTo(route.getTo());
        updateRoute.setDistance(route.getDistance());
        entityManager.flush();
    }

    @Override
    public Route get(long id) {
        return entityManager.find(Route.class, id);
    }

    @Override
    public Route get(long idFrom, long idTo) {
        TypedQuery<Route> query = entityManager.createQuery("Select c from Route c where c.from.id = :idFrom and c.to.id = :idTo", Route.class);
        query.setParameter("idFrom", idFrom).setParameter("idTo", idTo);
        return query.getSingleResult();
    }

    @Override
    public List<Route> getList(int start, int count) {
        TypedQuery<Route> query = entityManager.createQuery("Select c from Route c order by c.from.city asc", Route.class);
        query.setFirstResult(start);
        query.setMaxResults(count);
        return query.getResultList();
    }

    @Override
    public List<Route> getList(long id) {
        TypedQuery<Route> query = entityManager.createQuery("Select c from Route c where c.from.id = :id", Route.class);
        return query.setParameter("id", id).getResultList();
    }

    @Override
    public long count() {
        TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(c) FROM Route c", Long.class);
        return query.getSingleResult();
    }
}

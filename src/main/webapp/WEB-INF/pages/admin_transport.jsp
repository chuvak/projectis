<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cerulean/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-C++cugH8+Uf86JbNOnQoBweHHAe/wVKN/mb0lTybu/NZ9sEYbd+BbbYtNpWYAsNP" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <title>LogisticIS</title>
</head>
<body>
<div style="margin-left: 25%; margin-right: 25%; margin-top: 35px">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <button type="button" id="admin" class="btn btn-primary">Home</button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <button type="button" id="users" class="btn btn-primary">Users</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="transport" class="btn btn-primary">Transport</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="routes" class="btn btn-primary">Routes</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="depots" class="btn btn-primary">Depots</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="logisticData" class="btn btn-primary">Logistic Data</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="archive" class="btn btn-primary">Archive</button>
                </li>
            </ul>

            <button type="button" id="logout" class="btn btn-secondary">Logout</button>

        </div>
    </nav>

    <div style="margin-left: 20px; margin-right: 20px; margin-top: 20px; margin-bottom: 20px">
        <button type="button" id="newTransport" class="btn btn-secondary">New Transport</button>
    </div>

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Number</th>
            <th scope="col">Carrying Capacity</th>
            <th scope="col">Fuel Consumption</th>
            <th scope="col">Depot Location</th>
            <th scope="col">Depot Address</th>
            <th scope="col">Depot Phone</th>
            <th>    </th>
            <th>    </th>
        </tr>
        </thead>
        <tbody>

        <c:forEach items="${transportList}" var="transport">
            <tr>
                <td>${transport.number}</td>
                <td>${transport.carryingCapacity}</td>
                <td>${transport.fuelConsumption}</td>
                <c:choose>
                    <c:when test="${transport.depot ne null}">
                        <td>${transport.depot.city}</td>
                        <td>${transport.depot.address}</td>
                        <td>${transport.depot.phone}</td>
                    </c:when>
                    <c:otherwise>
                        <td>Default</td>
                        <td>Default</td>
                        <td>Default</td>
                    </c:otherwise>
                </c:choose>
                <td><a href="/admin/transport/edit?id=${transport.id}">Edit</a></td>
                <td><a href="/admin/transport/delete?id=${transport.id}">Delete</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div style="margin-left: 40%; margin-right: 40%">
        <ul class="pagination pagination-lg">
            <li class="page-item">
                <a class="page-link" href="/admin/transport/?page=<c:out value="${pages - 1}"/>">&laquo;</a>
            </li>
            <li class="page-item disabled">
                <a class="page-link" href="#"><c:out value="${pages}"/></a>
            </li>
            <li class="page-item">
                <a class="page-link" href="/admin/transport/?page=<c:out value="${pages + 1}"/>">&raquo;</a>
            </li>
        </ul>
    </div>

</div>

<script>
    $('#admin').click(function () {
        window.location.href = "/admin";
    });

    $('#users').click(function () {
        window.location.href = "/admin/users";
    });

    $('#transport').click(function () {
        window.location.href = "/admin/transport";
    });

    $('#routes').click(function () {
        window.location.href = "/admin/routes";
    });

    $('#depots').click(function () {
        window.location.href = "/admin/depots";
    });

    $('#logisticData').click(function () {
        window.location.href = "/admin/logisticData";
    });

    $('#archive').click(function () {
        window.location.href = "/admin/archive";
    });

    $('#logout').click(function () {
        window.location.href = "/logout";
    });

    $('#newTransport').click(function () {
        window.location.href = "/admin/transport/new";
    });
</script>
</body>
</html>

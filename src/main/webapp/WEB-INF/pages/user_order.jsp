<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cerulean/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-C++cugH8+Uf86JbNOnQoBweHHAe/wVKN/mb0lTybu/NZ9sEYbd+BbbYtNpWYAsNP" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <title>LogisticIS</title>
</head>
<body>
<div style="margin-left: 25%; margin-right: 25%; margin-top: 35px">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <button type="button" id="user" class="btn btn-primary">Home</button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <button type="button" id="orderManager" class="btn btn-primary">Order Manager</button>
                </li>
                <li class="nav-item active">
                    <button type="button" id="orderConfirmation" class="btn btn-primary">Order Confirmation</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="logisticData" class="btn btn-primary">Logistic Data</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="archive" class="btn btn-primary">Archive</button>
                </li>
            </ul>

            <button type="button" id="logout" class="btn btn-secondary">Logout</button>
        </div>
    </nav>

    <div class="row" style="margin-left: 20px; margin-right: 20px; margin-top: 20px; margin-bottom: 20px">
        <button type="button" id="newOrder" class="btn btn-secondary">New Order</button>
        <div style="margin-left: 6px">
            <c:url value="/user/orderConfirmation/search" var="search" />
            <form class="form-inline my-2 my-lg-0" action="${search}" method="post">
                <input class="form-control mr-sm-2" type="text" autocomplete="off" id="search" name="search" placeholder="Declaration Number">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
        <button style="margin-left: 6px" type="button" id="all" class="btn btn-secondary">All</button>
    </div>

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Shiping Date</th>
            <th scope="col">Declaration Number</th>
            <th scope="col">To</th>
            <th scope="col">Description</th>
            <th scope="col">Weight</th>
            <th scope="col">Cost</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${ordersListFrom}" var="logisticOrder">
            <tr>
                <td>${logisticOrder.shippingDate}</td>
                <td>${logisticOrder.declarationNumber}</td>
                <td>${logisticOrder.to.city}</td>
                <td>${logisticOrder.description}</td>
                <td>${logisticOrder.weight}</td>
                <td>${logisticOrder.cost}</td>
                <td><a href="/user/orderManager/delete?id=${logisticOrder.id}">Delete</a></td>
                <td><a target="_blank" href="/user/orderManager/print?id=${logisticOrder.id}">Print</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<script>
    $('#user').click(function () {
        window.location.href = "/user";
    });

    $('#orderManager').click(function () {
        window.location.href = "/user/orderManager";
    });

    $('#newOrder').click(function () {
        window.location.href = "/user/orderManager/new";
    });

    $('#orderConfirmation').click(function () {
        window.location.href = "/user/orderConfirmation";
    });

    $('#logisticData').click(function () {
        window.location.href = "/user/logisticData";
    });

    $('#archive').click(function () {
        window.location.href = "/user/archive";
    });

    $('#logout').click(function () {
        window.location.href = "/logout";
    });

    $('#all').click(function () {
        window.location.href = "/user/orderManager";
    });

</script>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cerulean/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-C++cugH8+Uf86JbNOnQoBweHHAe/wVKN/mb0lTybu/NZ9sEYbd+BbbYtNpWYAsNP" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <title>LogisticIS</title>

</head>
<body>
<div style="margin-left: 40%; margin-right: 40%; margin-top: 10%">
    <c:url value="/admin/routes/update" var="update"/>
    <form action="${update}" method="post">
        <fieldset>
            <input type="hidden" class="form-control" autocomplete="off" id="id" name="id" readonly value="${route.id}">

            <div class="form-group">
                <label for="depotFrom">From Depot</label>
                <select class="form-control" name="depotFrom" id="depotFrom">
                    <c:forEach items="${depotList}" var="depot">
                        <c:if test="${depot.id == route.from.id}">
                            <option value="${depot.id}">${depot.city}</option>
                        </c:if>
                    </c:forEach>
                    <c:forEach items="${depotList}" var="depot">
                        <c:if test="${depot.id != route.from.id}">
                            <option value="${depot.id}">${depot.city}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group">
                <label for="depotTo">To Depot</label>
                <select class="form-control" name="depotTo" id="depotTo">
                    <c:forEach items="${depotList}" var="depot">
                        <c:if test="${depot.id == route.to.id}">
                            <option value="${depot.id}">${depot.city}</option>
                        </c:if>
                    </c:forEach>
                    <c:forEach items="${depotList}" var="depot">
                        <c:if test="${depot.id != route.to.id}">
                            <option value="${depot.id}">${depot.city}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group">
                <label for="distance">Distance</label>
                <input type="text" class="form-control" autocomplete="off" id="distance" name="distance"
                       value="${route.distance}">
            </div>

            <c:if test="${param.error ne null}">
                <div class="alert alert-dismissible alert-danger">
                    <strong>Warning!</strong> Wrong input.
                </div>
            </c:if>

            <button type="button" id="cancel" class="btn btn-primary">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </fieldset>
    </form>
</div>

<script>
    $('#cancel').click(function () {
        window.location.href = "/admin/routes";
    });
</script>
</body>
</html>

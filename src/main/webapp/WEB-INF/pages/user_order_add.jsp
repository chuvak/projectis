<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cerulean/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-C++cugH8+Uf86JbNOnQoBweHHAe/wVKN/mb0lTybu/NZ9sEYbd+BbbYtNpWYAsNP" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <title>LogisticIS</title>

</head>
<body>
<div style="margin-left: 40%; margin-right: 40%; margin-top: 10%">
    <c:url value="/user/orderManager/add" var="add" />
    <form action="${add}" method="post">
        <fieldset>
            <legend>New Order</legend>

            <div class="form-group">
                <label for="depot">Transport</label>
                <select class="form-control" name="transport" id="transport">
                    <option value="-1">Select Transport</option>
                    <c:forEach items="${transport}" var="transport">
                        <option value="${transport.id}">${transport.number}, w:${transport.carryingCapacity}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group">
                <label for="depot">To</label>
                <select class="form-control" name="depot" id="depot">
                    <option value="-1">Select Depot</option>
                    <c:forEach items="${depots}" var="depot">
                        <option value="${depot.to.id}">${depot.to.city}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label for="weight">Weight</label>
                <input type="text" class="form-control" autocomplete="off" id="weight" name="weight"
                       placeholder="Enter Weight">
            </div>

            <c:if test="${param.error ne null}">
                <div class="alert alert-dismissible alert-danger">
                    <strong>Warning!</strong> Wrong input.
                </div>
            </c:if>

            <button type="button" id="cancel" class="btn btn-primary">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </fieldset>
    </form>
</div>

<script>
    $('#cancel').click(function () {
        window.location.href = "/user/orderManager";
    });
</script>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cerulean/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-C++cugH8+Uf86JbNOnQoBweHHAe/wVKN/mb0lTybu/NZ9sEYbd+BbbYtNpWYAsNP" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <title>LogisticIS</title>
</head>
<body>
<div style="margin-left: 20%; margin-right: 20%; margin-top: 35px">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <button type="button" id="user" class="btn btn-primary">Home</button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <button type="button" id="orderManager" class="btn btn-primary">Order Manager</button>
                </li>
                <li class="nav-item active">
                    <button type="button" id="orderConfirmation" class="btn btn-primary">Order Confirmation</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="logisticData" class="btn btn-primary">Logistic Data</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="archive" class="btn btn-primary">Archive</button>
                </li>
            </ul>

            <button type="button" id="logout" class="btn btn-secondary">Logout</button>

        </div>
    </nav>

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Shiping Date</th>
            <th scope="col">Receiving Date</th>
            <th scope="col">Transport Num</th>
            <th scope="col">From</th>
            <th scope="col">To</th>
            <th scope="col">Cost</th>
            <th scope="col">Declaration Num</th>
            <th scope="col">Order Cost</th>
            <th scope="col">Total Cost</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${archive}" var="archive">
            <tr>
                <td>${archive.shipingDate}</td>
                <td>${archive.receivingDate}</td>
                <td>${archive.transportNumber}</td>
                <td>${archive.from}</td>
                <td>${archive.to}</td>
                <td>${archive.cost}</td>
                <td>${archive.declarationNumber}</td>
                <td>${archive.orderCost}</td>
                <td>${archive.totalCost}</td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div style="margin-left: 40%; margin-right: 40%">
        <ul class="pagination pagination-lg">
            <li class="page-item">
                <a class="page-link" href="/admin/archive/?page=<c:out value="${pages + 1}"/>">&raquo;</a>
            </li>
            <li class="page-item disabled">
                <a class="page-link" href="#"><c:out value="${pages}"/></a>
            </li>
            <li class="page-item">
                <a class="page-link" href="/admin/archive/?page=<c:out value="${pages + 1}"/>">&raquo;</a>
            </li>
        </ul>
    </div>
</div>

<script>
    $('#user').click(function () {
        window.location.href = "/user";
    });

    $('#orderManager').click(function () {
        window.location.href = "/user/orderManager";
    });

    $('#orderConfirmation').click(function () {
        window.location.href = "/user/orderConfirmation";
    });

    $('#logisticData').click(function () {
        window.location.href = "/user/logisticData";
    });

    $('#archive').click(function () {
        window.location.href = "/user/archive";
    });

    $('#logout').click(function () {
        window.location.href = "/logout";
    });

</script>
</body>
</html>

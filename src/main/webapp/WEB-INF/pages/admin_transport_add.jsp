<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cerulean/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-C++cugH8+Uf86JbNOnQoBweHHAe/wVKN/mb0lTybu/NZ9sEYbd+BbbYtNpWYAsNP" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <title>LogisticIS</title>

</head>
<body>
<div style="margin-left: 40%; margin-right: 40%; margin-top: 10%">
    <c:url value="/admin/transport/add" var="add" />
    <form action="${add}" method="post">
        <fieldset>
            <legend>New Transport</legend>
            <div class="form-group">
                <label for="transportNumber">Number</label>
                <input type="text" class="form-control" autocomplete="off" id="transportNumber" name="transportNumber"
                       placeholder="Enter Number">
            </div>

            <div class="form-group">
                <label for="carryingCapacity">Carrying Capacity</label>
                <input type="text" class="form-control" autocomplete="off" id="carryingCapacity" name="carryingCapacity"
                       placeholder="Enter Carrying Capacity">
            </div>

            <div class="form-group">
                <label for="fuelConsumption">Fuel Consumption</label>
                <input type="text" class="form-control" autocomplete="off" id="fuelConsumption" name="fuelConsumption"
                       placeholder="Enter Fuel Сonsumption">
            </div>

            <div class="form-group">
                <label for="depot">Depot</label>
                <select class="form-control" name="depot" id="depot">
                    <option selected="">Select Depot</option>
                    <c:forEach items="${depots}" var="depot">
                        <option value="${depot.id}">${depot.city}</option>
                    </c:forEach>
                </select>
            </div>

            <c:if test="${param.error ne null}">
                <div class="alert alert-dismissible alert-danger">
                    <strong>Warning!</strong> Wrong input.
                </div>
            </c:if>

            <button type="button" id="cancel" class="btn btn-primary">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </fieldset>
    </form>
</div>

<script>
    $('#cancel').click(function () {
        window.location.href = "/admin/transport";
    });
</script>
</body>
</html>

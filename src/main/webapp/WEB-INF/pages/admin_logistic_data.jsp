<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cerulean/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-C++cugH8+Uf86JbNOnQoBweHHAe/wVKN/mb0lTybu/NZ9sEYbd+BbbYtNpWYAsNP" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>

    <title>LogisticIS</title>
</head>
<body>
<div style="margin-left: 25%; margin-right: 25%; margin-top: 35px">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <button type="button" id="admin" class="btn btn-primary">Home</button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <button type="button" id="users" class="btn btn-primary">Users</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="transport" class="btn btn-primary">Transport</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="routes" class="btn btn-primary">Routes</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="depots" class="btn btn-primary">Depots</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="logisticData" class="btn btn-primary">Logistic Data</button>
                </li>
                <li class="nav-item">
                    <button type="button" id="archive" class="btn btn-primary">Archive</button>
                </li>
            </ul>
            <button type="button" id="logout" class="btn btn-secondary">Logout</button>
        </div>
    </nav>

    <div style="margin-left: 25%; margin-right: 25%; margin-top: 50px">

        <table class="table table-hover">
            <tbody>
            <tr>
                <td width="40%"><strong>Fuel Cost:</strong></td>
                <td width="40%">${fuelCost.fuelCost}</td>
                <td width="20%"><a href="/admin/logisticData?editFuelCost">Edit</a></td>
            </tr>
            </tbody>
        </table>
        <c:if test="${param.editFuelCost ne null}">
            <c:url value="/admin/logisticData/updateFuelCost" var="editFC"/>
            <form action="${editFC}" method="post">
                <fieldset>
                    <div class="form-group">
                        <input type="text" class="form-control" autocomplete="off" id="fuelCost" name="fuelCost"
                               value="${fuelCost.fuelCost}">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary" style="margin-top: 12px">Submit</button>
                        <button type="button" id="logisticDataFuelCost" class="btn btn-primary"
                                style="margin-top: 12px">Cancel
                        </button>
                    </div>
                </fieldset>
            </form>
        </c:if>
        <table class="table table-hover">
            <tbody>
            <tr>
                <td width="40%"><strong>Cost Per Kilo:</strong></td>
                <td width="40%">${costPerKilo.costPerKilo}</td>
                <td width="20%"><a href="/admin/logisticData?editCostPerKilo">Edit</a></td>
            </tr>
            </tbody>
        </table>
        <c:if test="${param.editCostPerKilo ne null}">
            <c:url value="/admin/logisticData/updateCostPerKilo" var="editCPK"/>
            <form action="${editCPK}" method="post">
                <fieldset>
                    <div class="form-group">
                        <input type="text" class="form-control" autocomplete="off" id="costPerKilo" name="costPerKilo"
                               value="${costPerKilo.costPerKilo}">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary" style="margin-top: 12px">Submit</button>
                        <button type="button" id="logisticDataCostPerKilo" class="btn btn-primary"
                                style="margin-top: 12px">Cancel
                        </button>
                    </div>
                </fieldset>
            </form>
        </c:if>
    </div>
</div>

<script>
    $('#add').click(function () {
        window.location.href = "/admin/logisticData/new";
    });

    $('#admin').click(function () {
        window.location.href = "/admin";
    });

    $('#users').click(function () {
        window.location.href = "/admin/users";
    });

    $('#transport').click(function () {
        window.location.href = "/admin/transport";
    });

    $('#routes').click(function () {
        window.location.href = "/admin/routes";
    });

    $('#depots').click(function () {
        window.location.href = "/admin/depots";
    });

    $('#logisticDataFuelCost').click(function () {
        window.location.href = "/admin/logisticData";
    });

    $('#logisticDataCostPerKilo').click(function () {
        window.location.href = "/admin/logisticData";
    });

    $('#archive').click(function () {
        window.location.href = "/admin/archive";
    });

    $('#logout').click(function () {
        window.location.href = "/logout";
    });
</script>
</body>
</html>
